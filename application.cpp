#include "application.h"

#define _WIN32_WINNT 0x0500
#include <windows.h>

Application::Application()
{
    hideCursor();
    resizeWindow();
}

void Application::run()
{
    while (!m_exit)
    {
        if (m_gameScene == nullptr)
            m_gameScene = new GameScene();

        m_gameScene->show();

        delete m_gameScene;
        m_gameScene = nullptr;
    }
}

void Application::hideCursor() const
{
    HANDLE hConsoleOutput = GetStdHandle( STD_OUTPUT_HANDLE );
    CONSOLE_CURSOR_INFO structCursorInfo;
    GetConsoleCursorInfo( hConsoleOutput, &structCursorInfo );
    structCursorInfo.bVisible = FALSE;
    SetConsoleCursorInfo( hConsoleOutput, &structCursorInfo );
}

void Application::resizeWindow() const
{
    std::string command = "MODE CON COLS=" + m_CONSOLE_COLS_COUNT + " LINES=" + m_CONSOLE_LINES_COUNT;
    system(command.c_str());

    HWND console = GetConsoleWindow();
    RECT r;
    GetWindowRect(console, &r);

    MoveWindow(console, r.left, r.top, m_WIDTH, m_HEIGHT, TRUE);
}
