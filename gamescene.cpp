#include "gamescene.h"

#include <iostream>
#include <conio.h>

#include <windows.h>

GameScene::GameScene()
{
    m_playerBoard.generate();
    m_aiBoard.generate();
}

void GameScene::render()
{
    using std::cout;
    using std::endl;

    for (int i = 5; i < 39; ++i)
    {
        setCursorPosition(i, 40); std::cout << "|";
    }

    setCursorPosition(10, 13); cout << "Opponent Board";
    setCursorPosition(12, 14); cout << "+----------+" << endl;
    setCursorPosition(13, 14);


    for (int i = 0; i < 10; ++i)
    {
        cout << "|";
        for (int j = 0; j < 10; ++j)
        {
            cout << m_aiBoardCopy.fieldValue(i, j);
        }

        cout << "|";
        setCursorPosition(13 + i + 1, 14);
    }

    setCursorPosition(23, 14); cout << "+----------+" << endl;

    setCursorPosition(m_playerPosition);
    printInColor(LightBlue, ' ');

    setCursorPosition(26, 10);
    cout << "Ships left: ";    cout << m_aiBoard.shipsLeft(Board::Ship4) << "x oooo";
    setCursorPosition(27, 22); cout << m_aiBoard.shipsLeft(Board::Ship3) << "x ooo";
    setCursorPosition(28, 22); cout << m_aiBoard.shipsLeft(Board::Ship2) << "x oo";
    setCursorPosition(29, 22); cout << m_aiBoard.shipsLeft(Board::Ship1) << "x o";


    setCursorPosition(10, 55); cout << "Your Board";

    setCursorPosition(12, 54); cout << "+----------+" << endl;
    setCursorPosition(13, 54);

    for (int i = 0; i < 10; ++i)
    {
        cout << "|";
        for (int j = 0; j < 10; ++j)
        {
            cout << m_playerBoard.fieldValue(i, j);
        }

        cout << "|";
        setCursorPosition(13 + i + 1, 54);
    }

    setCursorPosition(23, 54); cout << "+----------+" << endl;

    setCursorPosition(26, 50);
    cout << "Ships left: ";    cout << m_playerBoard.shipsLeft(Board::Ship4) << "x oooo";
    setCursorPosition(27, 62); cout << m_playerBoard.shipsLeft(Board::Ship3) << "x ooo";
    setCursorPosition(28, 62); cout << m_playerBoard.shipsLeft(Board::Ship2) << "x oo";
    setCursorPosition(29, 62); cout << m_playerBoard.shipsLeft(Board::Ship1) << "x o";

}

void GameScene::handleInput()
{
    int input = getch();

    if (kbhit())
        input = getch();

    switch (input)
    {
    case Esc:
        break;

    case Up:
        if (m_playerPosition.x <= m_MIN_X)
            return;

        clear();
        m_playerPosition.x--;
        break;

    case Down:
        if (m_playerPosition.x >= m_MAX_X - 1)
            return;

        clear();
        m_playerPosition.x++;
        break;

    case Left:
        if (m_playerPosition.y <= m_MIN_Y)
            return;

        clear();
        m_playerPosition.y--;
        break;

    case Right:
        if (m_playerPosition.y >= m_MAX_Y - 1)
            return;

        clear();
        m_playerPosition.y++;
        break;

    case Enter:
        makePlayerMove();
        break;
    }

}

void GameScene::update()
{
    setCursorPosition(m_playerPosition);
    printInColor(LightBlue, m_aiBoardCopy.fieldValue(scaleConsoleToBoard(m_playerPosition)));

    setCursorPosition(26, 22); std::cout << m_aiBoard.shipsLeft(Board::Ship4);
    setCursorPosition(27, 22); std::cout << m_aiBoard.shipsLeft(Board::Ship3);
    setCursorPosition(28, 22); std::cout << m_aiBoard.shipsLeft(Board::Ship2);
    setCursorPosition(29, 22); std::cout << m_aiBoard.shipsLeft(Board::Ship1);

    drawPlayerPrompt();

    if (m_hitState == HitState::HitAndSunk)
    {
        if (m_aiBoard.isEndOfShips())
        {
            setCursorPosition(32, 9); std::cout << "+---------------------+";
            setCursorPosition(33, 9); std::cout << "|       YOU WIN!      |";
            setCursorPosition(34, 9); std::cout << "+---------------------+";

            setCursorPosition(36, 8); std::cout << "Press Enter to play again";

            while (getch() != Key::Enter) { };

            setCursorPosition(32, 9); std::cout << "                       ";
            setCursorPosition(33, 9); std::cout << "                       ";
            setCursorPosition(34, 9); std::cout << "                       ";

            setCursorPosition(36, 8); std::cout << "                         ";

            m_exit = true;
            return;
        }
    }

    if (m_hitState == HitState::Missed)
        m_currentTurn = Turn::Opponent;

    if (m_currentTurn == Turn::Opponent)
    {
        m_hitState = HitState::None;

        do
        {
            makeOpponentMove();

            setCursorPosition(m_opponentPosition);
            std::cout << m_playerBoard.fieldValue(m_opponentPosition - Point(13, 55));
            setCursorPosition(m_opponentPosition);
            printInColor(LightBlue, m_playerBoardCopy.fieldValue(m_opponentPosition - Point(13, 55)));;

            setCursorPosition(26, 62); std::cout << m_playerBoard.shipsLeft(Board::Ship4);
            setCursorPosition(27, 62); std::cout << m_playerBoard.shipsLeft(Board::Ship3);
            setCursorPosition(28, 62); std::cout << m_playerBoard.shipsLeft(Board::Ship2);
            setCursorPosition(29, 62); std::cout << m_playerBoard.shipsLeft(Board::Ship1);

            if (m_playerBoard.isEndOfShips())
            {
                setCursorPosition(32, 50); std::cout << "+---------------------+";
                setCursorPosition(33, 50); std::cout << "|      YOU LOSE!      |";
                setCursorPosition(34, 50); std::cout << "+---------------------+";

                setCursorPosition(36, 49); std::cout << "Press Enter to play again";

                while (getch() != Key::Enter) { };

                setCursorPosition(32, 50); std::cout << "                       ";
                setCursorPosition(33, 50); std::cout << "                       ";
                setCursorPosition(34, 50); std::cout << "                       ";

                setCursorPosition(36, 49); std::cout << "                         ";

                m_exit = true;
                return;
            }

            drawOpponentPrompt();

            setCursorPosition(m_opponentPosition);
            printInColor(Black, m_playerBoardCopy.fieldValue(m_opponentPosition - Point(13, 55)));

        }
        while (m_hitState != HitState::Missed);


        m_currentTurn = Turn::Player;
    }

    m_hitState = HitState::None;
}

void GameScene::printInColor(const Scene::Color &color, const char &value) const
{
    HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
    CONSOLE_SCREEN_BUFFER_INFO csbi;

    GetConsoleScreenBufferInfo(hConsole, &csbi);
    SetConsoleTextAttribute(hConsole, (csbi.wAttributes & 0xFF0F) | (((WORD)color) << 4));

    std::cout << value;

    CONSOLE_SCREEN_BUFFER_INFO csbi2;
    GetConsoleScreenBufferInfo(hConsole, &csbi2);
    SetConsoleTextAttribute(hConsole, (csbi2.wAttributes & 0xFF0F) | (((WORD)Black) << 4));
}

void GameScene::clear() const
{
    setCursorPosition(m_playerPosition);
    printInColor(Black, m_aiBoardCopy.fieldValue(scaleConsoleToBoard(m_playerPosition)));
}

void GameScene::makePlayerMove()
{
    if (m_aiBoardCopy.fieldValue(scaleConsoleToBoard(m_playerPosition)) != Board::Empty)
        return;

    if (m_aiBoard.fieldValue(scaleConsoleToBoard(m_playerPosition)) == Board::Empty)
    {
        m_aiBoardCopy.setFieldValue(scaleConsoleToBoard(m_playerPosition), Board::Missed);

        m_hitState = HitState::Missed;
    }
    else if (m_aiBoard.fieldValue(scaleConsoleToBoard(m_playerPosition)) == Board::Ship)
    {
        m_aiBoardCopy.setFieldValue(scaleConsoleToBoard(m_playerPosition), Board::Sunk);

        if (!m_aiBoard.deletePartOfShip(scaleConsoleToBoard(m_playerPosition)))
            m_hitState = HitState::Hit;
        else
            m_hitState = HitState::HitAndSunk;
    }
}

void GameScene::makeOpponentMove()
{
    Point position;
    bool repeat = true;

    static bool searchInNeighbour = false;
    static short aiming = 0;
    static Point lastHitPosition {};
    static Point aimingHitPosition {};

    while (repeat)
    {

        if (searchInNeighbour)
        {
            if (aiming >= 2)
            {
                short startPos = 0;

                if (aimingHitPosition.x == lastHitPosition.x)
                {
                    position.x = lastHitPosition.x;
                    position.y = (rand() % m_playerBoardCopy.getPossibleRange(aimingHitPosition, true, startPos)) + startPos;
                }
                else
                {
                    position.x = (rand() % m_playerBoardCopy.getPossibleRange(aimingHitPosition, false, startPos)) + startPos;
                    position.y = lastHitPosition.y;
                }

                if (m_playerBoardCopy.fieldValue(position) != Board::Empty)
                    continue;
            }
            else
            {
                position.x = (rand() % 3) + (lastHitPosition.x - 1);

                if (position.x == lastHitPosition.x)
                    position.y = (rand() % 3) + (lastHitPosition.y - 1);
                else
                    position.y = lastHitPosition.y;

                if (position == lastHitPosition)
                    continue;
            }
        }
        else
        {
            position.x = rand() % Board::SIZE;
            position.y = rand() % Board::SIZE;

            if (m_playerBoardCopy.searchFor(Board::Sunk, position) &&
                    m_playerBoard.fieldValue(position) != Board::Ship)
                continue;
        }


        if (m_playerBoardCopy.fieldValue(position) == Board::Empty)
            repeat = false;
    }

    if (m_playerBoard.fieldValue(position) == Board::Empty)
    {
        m_playerBoardCopy.setFieldValue(position, Board::Missed);
        m_hitState = HitState::Missed;
    }

    else if (m_playerBoard.fieldValue(position) == Board::Ship)
    {
        m_playerBoardCopy.setFieldValue(position, Board::Sunk);

        if (!m_playerBoard.deletePartOfShip(position))
        {
            searchInNeighbour = true;

            aiming++;
            if (aiming == 2)
                aimingHitPosition = lastHitPosition;

            lastHitPosition = position;

            m_hitState = HitState::Hit;
        }
        else
        {
            searchInNeighbour = false;
            aiming = 0;
            m_hitState = HitState::HitAndSunk;
        }
    }

    position += Point(13, 55);
    m_opponentPosition = position;
}

void GameScene::drawPlayerPrompt() const
{
    switch (m_hitState)
    {
    case HitState::Missed:
        setCursorPosition(32, 9); std::cout << "+---------------------+";
        setCursorPosition(33, 9); std::cout << "|        MISSED!      |";
        setCursorPosition(34, 9); std::cout << "+---------------------+";

        setCursorPosition(36, 9); std::cout << "Press Enter to continue";

        while (getch() != Key::Enter) { };

        setCursorPosition(32, 9); std::cout << "                       ";
        setCursorPosition(33, 9); std::cout << "                       ";
        setCursorPosition(34, 9); std::cout << "                       ";

        setCursorPosition(36, 9); std::cout << "                       ";

        break;

    case HitState::Hit:
        setCursorPosition(32, 9); std::cout << "+---------------------+";
        setCursorPosition(33, 9); std::cout << "|         HIT!        |";
        setCursorPosition(34, 9); std::cout << "+---------------------+";
        break;

    case HitState::HitAndSunk:
        setCursorPosition(32, 9); std::cout << "+---------------------+";
        setCursorPosition(33, 9); std::cout << "|     HIT AND SUNK!   |";
        setCursorPosition(34, 9); std::cout << "+---------------------+";
        break;

    default:
        break;
    }
}

void GameScene::drawOpponentPrompt() const
{
    switch (m_hitState)
    {
    case HitState::Missed:
        setCursorPosition(32, 50); std::cout << "+---------------------+";
        setCursorPosition(33, 50); std::cout << "|        MISSED!      |";
        setCursorPosition(34, 50); std::cout << "+---------------------+";

        break;

    case HitState::Hit:
        setCursorPosition(32, 50); std::cout << "+---------------------+";
        setCursorPosition(33, 50); std::cout << "|         HIT!        |";
        setCursorPosition(34, 50); std::cout << "+---------------------+";
        break;

    case HitState::HitAndSunk:
        setCursorPosition(32, 50); std::cout << "+---------------------+";
        setCursorPosition(33, 50); std::cout << "|     HIT AND SUNK!   |";
        setCursorPosition(34, 50); std::cout << "+---------------------+";
        break;

    default:
        break;
    }

    setCursorPosition(36, 50); std::cout << "Press Enter to continue";

    while (getch() != Key::Enter) { };

    setCursorPosition(32, 50); std::cout << "                       ";
    setCursorPosition(33, 50); std::cout << "                       ";
    setCursorPosition(34, 50); std::cout << "                       ";

    setCursorPosition(36, 50); std::cout << "                       ";
}

Point GameScene::scaleConsoleToBoard(const Point &point) const
{
    return Point(point.x - m_MIN_X, point.y - m_MIN_Y);
}

Point GameScene::scaleBoardToConsole(const Point &point) const
{
    return Point(point.x + m_MIN_X, point.y + m_MIN_Y);
}
