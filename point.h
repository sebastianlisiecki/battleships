#ifndef POINT_H
#define POINT_H

struct Point
{
    short x;
    short y;

    Point(const short& _x = 0, const short& _y = 0) :
        x(_x), y(_y) {}

    bool operator ==(const Point& other)
    {
        return (x == other.x && y == other.y);
    }

    Point& operator +=(const Point& other)
    {
        x += other.x;
        y += other.y;

        return *this;
    }

    Point operator -(const Point& other)
    {
        return Point(x - other.x, y - other.y);
    }

    Point operator +(const Point& other)
    {
        return Point(x + other.x, y + other.y);
    }
};


#endif // POINT_H
