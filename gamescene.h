#ifndef GAMESCENE_H
#define GAMESCENE_H

#include "scene.h"

class GameScene : public Scene
{
public:
    GameScene();

protected:
    void render() override;
    void handleInput() override;
    void update() override;

private:

    enum class Turn
    {
        Player,
        Opponent
    };

    enum class HitState
    {
        None,
        Missed,
        Hit,
        HitAndSunk
    };


    void printInColor(const Color& color, const char& value) const;

    void clear() const;
    void makePlayerMove();
    void makeOpponentMove();
    void drawPlayerPrompt() const;
    void drawOpponentPrompt() const;

    Point scaleConsoleToBoard(const Point& point) const;
    Point scaleBoardToConsole(const Point& point) const;

    Board m_playerBoard;
    Board m_playerBoardCopy;
    Board m_aiBoard;
    Board m_aiBoardCopy;

    const short m_MIN_X = 13;
    const short m_MIN_Y = 15;
    const short m_MAX_X = m_MIN_X + Board::SIZE;
    const short m_MAX_Y = m_MIN_Y + Board::SIZE;

    Point m_playerPosition {m_MIN_X, m_MIN_Y};
    Point m_opponentPosition {13, 55};

    Turn m_currentTurn = Turn::Player;
    HitState m_hitState = HitState::None;

};

#endif // GAMESCENE_H
