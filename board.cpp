#include "board.h"

#include <ctime>
#include <iostream>
#include <algorithm>

const int Board::SIZE = 10;

Board::Board() :
    m_field(SIZE, std::vector<FieldValue>(SIZE, Empty)),
    m_shipsCount { {Ship4, 1}, {Ship3, 2}, {Ship2, 3}, {Ship1, 4} }
{
    srand(time(nullptr));

    for (auto i : m_shipsCount)
    {
        m_shipsLeftAmount[i.first] = i.second;
    }
}

void Board::generate()
{
    std::vector<std::pair<ShipType, int>> shipsToGenerate(m_shipsCount);

    for (auto& i : shipsToGenerate)
    {
        int counter = 0;
        while (i.second > 0)
        {
            short x, y;

            if (i.first == Ship1)
            {
                x = rand() % SIZE;
                y = rand() % SIZE;

                if (!checkCorrectness(x, y))
                    continue;

                m_field[x][y] = Ship;
                m_ships[i.first].push_back(std::vector<Point>{ Point(x, y) });
                i.second--;
            }
            else
            {
                bool horizontal = rand() % 2;
                bool ok = true;

                if (horizontal)
                {
                    x = rand() % SIZE;
                    y = rand() % (SIZE - i.first - 1);

                    for (int j = y; j < y + i.first; ++j)
                    {
                        if (!checkCorrectness(x, j))
                        {
                            ok = false;
                            break;
                        }
                    }

                    if (!ok)
                        continue;

                    m_ships[i.first].push_back(std::vector<Point>());


                    for (int j = y; j < y + i.first; ++j)
                    {
                        m_field[x][j] = Ship;
                        m_ships[i.first][counter].push_back(Point(x, j));
                    }

                    i.second--;
                    counter++;
                }
                else
                {
                    x = rand() % (SIZE - i.first - 1);
                    y = rand() % SIZE;

                    for (int j = x; j < x + i.first; ++j)
                    {
                        if (!checkCorrectness(j, y))
                        {
                            ok = false;
                            break;
                        }
                    }

                    if (!ok)
                        continue;

                    m_ships[i.first].push_back(std::vector<Point>());

                    for (int j = x; j < x + i.first; ++j)
                    {
                        m_field[j][y] = Ship;
                        m_ships[i.first][counter].push_back(Point(j, y));
                    }

                    i.second--;
                    counter++;
                }
            }
        }

        counter = 0;
    }
}

char Board::fieldValue(const int& x, const int& y) const
{
    return m_field[x][y];
}

char Board::fieldValue(const Point &point) const
{
    return m_field[point.x][point.y];
}

void Board::setFieldValue(const Point &point, const Board::FieldValue &value)
{
    m_field[point.x][point.y] = value;
}

int Board::shipsLeft(const Board::ShipType &type) const
{
    return m_shipsLeftAmount.at(type);
}

int Board::ship1Left() const
{
    return m_shipsLeftAmount.at(Ship1);
}

int Board::ship2Left() const
{
    return m_shipsLeftAmount.at(Ship2);
}

int Board::ship3Left() const
{
    return m_shipsLeftAmount.at(Ship3);
}

int Board::ship4Left() const
{
    return m_shipsLeftAmount.at(Ship4);
}

bool Board::deletePartOfShip(const Point &point)
{
    for (auto& ships : m_ships)
    {
        for (auto& ship : ships.second)
        {
            for (auto part = ship.begin(); part != ship.end(); ++part)
            {
                if (*part == point)
                {
                    ship.erase(part);

                    if (ship.empty())
                    {
                        m_shipsLeftAmount[ships.first]--;
                        return true;
                    }

                    return false;
                }
            }
        }
    }

    return false;
}

bool Board::isEndOfShips() const
{
    for (auto ships : m_ships)
    {
        for (auto ship : ships.second)
        {
            if (!ship.empty())
                return false;
        }
    }

    return true;
}

bool Board::searchFor(const FieldValue& type, const Point &point) const
{
    for (int i = -1; i <= 1; ++i)
    {
        for (int j = -1; j <= 1; ++j)
        {
            if (point.x <= 0 && i < 0)
                continue;

            if (point.x >= SIZE-1 && i > 0)
                continue;

            if (point.y <= 0 && j < 0)
                continue;

            if (point.y >= SIZE-1 && j > 0)
                continue;

            if (m_field[point.x+i][point.y+j] == type)
                return true;
        }
    }

    return false;
}

short Board::getPossibleRange(const Point& position, bool horizontal, short& outStartPos) const
{
    Point startPos = position;
    short counter = 0;

    if (horizontal)
    {
        if (startPos.y == 0)
            outStartPos = 0;
        else
        {
            while (m_field[startPos.x][startPos.y - 1] == Board::Sunk)
            {
                if (startPos.y == 1)
                    break;

                startPos.y--;
            }

            startPos.y--;
            outStartPos = startPos.y;
        }

        while (m_field[startPos.x][startPos.y + 1] == Board::Sunk)
        {
            if (startPos.y == SIZE - 2)
                break;

            startPos.y++;
            counter++;
        }

        counter += 2;

    }
    else
    {
        if (startPos.x == 0)
            outStartPos = 0;
        else
        {
            while (m_field[startPos.x - 1][startPos.y] == Board::Sunk)
            {
                if (startPos.x == 1)
                    break;

                startPos.x--;
            }

            startPos.x--;
            outStartPos = startPos.x;
        }

        while (m_field[startPos.x + 1][startPos.y] == Board::Sunk)
        {
            if (startPos.x == SIZE - 2)
                break;

            startPos.x++;
            counter++;
        }

        counter += 2;
    }

    return counter;
}

bool Board::checkCorrectness(const int &x, const int &y)
{
    for (int i = -1; i <= 1; ++i)
    {
        for (int j = -1; j <= 1; ++j)
        {
            if (x <= 0 && i < 0)
                continue;

            if (x >= SIZE-1 && i > 0)
                continue;

            if (y <= 0 && j < 0)
                continue;

            if (y >= SIZE-1 && j > 0)
                continue;

            if (m_field[x+i][y+j] != Empty)
                return false;
        }
    }

    return true;
}
