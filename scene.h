#ifndef SCENE_H
#define SCENE_H

#include "board.h"
#include "point.h"

class Scene
{
public:
    virtual ~Scene() {}

    void show();

protected:
    enum Color
    {
        Black,
        Blue,
        Green,
        Cyan,
        Red,
        Magneta,
        Brown,
        LightGray,
        DarkGray,
        LightBlue,
        LightGreen,
        LightCyan,
        LightRed,
        LightMagneta,
        Yellow,
        White
    };

    enum Key
    {
        Esc = 27,
        Up = 72,
        Down = 80,
        Left = 75,
        Right = 77,
        Enter = 13
    };

    virtual void render() = 0;
    virtual void handleInput() = 0;
    virtual void update() = 0;

    void setCursorPosition(const Point& point) const;
    void setCursorPosition(const short &x, const short &y) const;

    bool m_exit = false;

private:
    void drawConstantGraphics() const;

};

#endif // SCENE_H
