#ifndef BOARD_H
#define BOARD_H

#include <vector>
#include <map>

#include "point.h"

class Board
{
public:
    Board();

    enum FieldValue
    {
        Empty = ' ',
        Ship = 'o',
        Missed = '.',
        Sunk = 'x'
    };

    enum ShipType
    {
        None,
        Ship1,
        Ship2,
        Ship3,
        Ship4,
    };

    void generate();

    char fieldValue(const int& x, const int& y) const;
    char fieldValue(const Point& point) const;

    void setFieldValue(const Point& point, const FieldValue& value);

    int shipsLeft(const ShipType& type) const;

    int ship1Left() const;
    int ship2Left() const;
    int ship3Left() const;
    int ship4Left() const;

    bool deletePartOfShip(const Point& point);
    bool isEndOfShips() const;

    bool searchFor(const FieldValue &type, const Point& point) const;
    short getPossibleRange(const Point &position, bool horizontal, short& outStartPos) const;

    static const int SIZE;

private:
    bool checkCorrectness(const int& x, const int& y);

    std::vector<std::vector<FieldValue>> m_field;
    const std::vector<std::pair<ShipType, int>> m_shipsCount;
    std::map<ShipType, int> m_shipsLeftAmount;

    std::map<ShipType, std::vector<std::vector<Point>>> m_ships;

};

#endif // BOARD_H
