#ifndef APPLICATION_H
#define APPLICATION_H

#include "gamescene.h"

#include <string>

class Application
{
public:
    Application();

    void run();

private:

    void hideCursor() const;
    void resizeWindow() const;

    GameScene* m_gameScene = nullptr;

    const int m_WIDTH = 800;
    const int m_HEIGHT = 511;

    const std::string m_CONSOLE_COLS_COUNT = "80";
    const std::string m_CONSOLE_LINES_COUNT = "40";

    bool m_exit = false;
};

#endif // APPLICATION_H
