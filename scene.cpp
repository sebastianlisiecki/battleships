#include "scene.h"

#include <iostream>
#include <windows.h>

void Scene::show()
{
    drawConstantGraphics();
    render();

    while (!m_exit)
    {
        handleInput();
        update();
    }
}

void Scene::setCursorPosition(const Point& point) const
{
    COORD p = { point.y, point.x };
    SetConsoleCursorPosition( GetStdHandle( STD_OUTPUT_HANDLE ), p );
}

void Scene::setCursorPosition(const short& x, const short& y) const
{
    COORD p = { y, x };
    SetConsoleCursorPosition( GetStdHandle( STD_OUTPUT_HANDLE ), p );
}

void Scene::drawConstantGraphics() const
{
    using std::cout;
    using std::endl;

    setCursorPosition(0, 0); cout  << " ---------------------------";
    setCursorPosition(0, 28); cout << "+----------------------+";
    setCursorPosition(1, 28); cout << "|                      |";
    setCursorPosition(2, 28); cout << "|      BATTLESHIPS     |";
    setCursorPosition(3, 28); cout << "|                      |";
    setCursorPosition(4, 28); cout << "+----------------------+";
    setCursorPosition(0, 52); cout << "--------------------------- ";

    for (int i = 1; i < 39; ++i)
    {
        setCursorPosition(i, 0); cout << "|";
        setCursorPosition(i, 79); cout << "|";
    }

    setCursorPosition(39, 0); cout <<" ------------------------------------------------------------------------------";
}
